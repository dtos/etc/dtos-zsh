#+TITLE: DTOS Configs
#+DESCRIPTION: A repository of default configuration files for DTOS.
#+AUTHOR: Derek Taylor (DistroTube)

* What programs use this repo?
The following DTOS packages have their configs hosted here:
+ dtos-alacritty
+ dtos-backgrounds
+ dtos-bash
+ dtos-conky
+ dtos-doom
+ dtos-fish
+ dtos-local-bin
+ dtos-opendoas
+ dtos-sxiv
+ dtos-xmobar
+ dtos-xmonad
+ dtos-xresources
+ dtos-xwallpaper
+ dtos-zsh

* Installing these programs
You must have the DTOS core repo enabled on an Arch Linux-based system to install these.  When you install these programs, they don't install executable programs.  These programs are just configuration files.  These configuration files get placed in /etc/dtos.  For example, if you install dtos-bash, it places the DTOS .bashrc configuration file in /etc/dtos.  It doesn't place the .bashrc in your $USER's $HOME. You must manually move it from /etc/dtos over to your $HOME.

=NOTE:= Most of the dtos-* packages will install the programs that the configs depend on.  For example, dtos-fish will also install fish since it is a dependency.  dtos-alacritty will install the alacritty terminal, etc.
